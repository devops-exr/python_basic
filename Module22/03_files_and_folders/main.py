import os


def function(way):
    cnt_catalogs = 0
    cnt_files = 0
    total_size = 0
    for i in os.listdir(way):
        path = os.path.join(way, i)
        total_size += os.path.getsize(path)
        if os.path.isdir(path):
            # print("It`s DIR -> ", path)
            cnt_catalogs += 1
            cnt_catalog, cnt_file, total_s = function(path)
            cnt_files += cnt_file
            cnt_catalogs += cnt_catalog
            total_size += total_s
        elif os.path.isfile(path):
            # print("It`s FILE -> ", path)
            cnt_files += 1

    return cnt_catalogs, cnt_files, total_size


way = os.path.abspath("..")
dirs, file, size = function(way)

print("Размер каталога (в Кбайтах): ", size / 1024)
print("Количество подкаталогов: ", dirs)
print("Количество файлов: ", file)
