winners = []
tmp = []
write_win = []
first_file = open('first_tour.txt', 'r', encoding="utf8")
f_wr = open("second_tour.txt", "w", encoding="utf8")
minn = int(first_file.readline())
for i in first_file:
    score = i.split()
    score_p = score[2]
    if int(score_p) > int(minn):
        member = [score[1][0], score[0], score[2]]
        winners.append(member)

for i in winners:
    tmp.append(int(i[2]))

sort_tmp = sorted(tmp, reverse=True)

for i in sort_tmp:
    for j, v in enumerate(winners):
        if int(v[2]) == int(i):
            member = [v[0], v[1], v[2]]
            write_win.append(member)

for i, val in enumerate(write_win, 1):
    f_wr.write(f'{i}) {val[0]}. {val[1]} {val[2]}\n')

first_file.close()
f_wr.close()