from datetime import datetime

current_datetime = datetime.now().strftime("%d-%m-%Y %H-%M-%S")

def login(part_txt):
    while True:
        with open(part_txt, 'r', encoding='utf8') as read_part:
            person = input("Введите имя пользователя: ")
            for j in read_part.readlines():
                if person.strip() == j.strip():
                    print(j)
                    return person

part_txt = 'participants.txt'

per = login(part_txt)
while True:
    try:
        with (open('chat.txt', 'r', encoding='utf8') as read_chat,
              open('chat.txt', 'a', encoding='utf8') as writting_chat):


            print('''Выберете действие:
             1. Посмотреть текущий текст чата.
             2. Отправить сообщение в чат.
             3. Сменить пользователя.
             4. EXIT''')
            choice = int(input("Введите номер подходящего варианта: "))
            if choice == 1:
                for i in read_chat:
                    print(i, end="")

            elif choice == 2:
                msg = input("Type: ")
                writting_chat.write(per + ": " + msg + ("\t" * 5) + current_datetime + "\n")
            elif choice == 3:
                per = login(part_txt)
            else:
                print("Пока,", per)
                break
            print()

    except FileNotFoundError:
        print("Файла не существует")
        break
    except ValueError as exc:
        print("ValueError", exc)
        break
