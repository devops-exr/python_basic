import math

def get_sage_sqrt(number):
    try:
        n = math.sqrt(number)
        if n == 0:
            raise ValueError('Число "0"')
        elif n.is_integer():
            return n
        elif n != 0:
            return round(n, 3)
    except TypeError as exc:
        return "TypeError", exc
    except ValueError as exc:
        return "ValueError", exc


numbers = [16, 25, -9, 0, 4.5, "abc"]
for number in numbers:
    result = get_sage_sqrt(number)
    print(f"Квадратный корень numbers {number}: {result}")
