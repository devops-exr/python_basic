def op_file():
    with (open('registrations.txt', 'r', encoding='utf8') as file,
          open('registrations_bad.log', 'w', encoding='utf8') as bad,
          open('registrations_good.log', 'w', encoding='utf8') as good):
        for i in file.readlines():
            try:
                tmp = i.split()
                ln = len(i)
                if ln < 3:
                    raise IndexError
                elif not tmp[0].isalpha():
                    raise NameError
                elif '@' and '.' not in tmp[1]:
                    raise SyntaxError
                elif int(tmp[2]) > 99 or int(tmp[2]) < 10:
                    raise ValueError
            except IndexError:
                bad.write(str(i.rstrip()) + "\t НЕ присутствуют все три поля: IndexError\n")
            except NameError:
                bad.write(str(i.rstrip()) + "\t Поле «Имя» содержит НЕ только буквы: NameError\n")
            except SyntaxError:
                bad.write(str(i.rstrip()) + "\t Поле «Имейл» НЕ содержит @ и точку: SyntaxError\n")
            except ValueError:
                bad.write(str(i.rstrip()) + "\t Поле «Возраст» НЕ представляет число от 10 до 99: ValueError\n")
            else:
                good.write(str(i))



op_file()
