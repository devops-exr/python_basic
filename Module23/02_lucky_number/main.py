import random

# def rand_error():
#     num = random.randint(1,3)
#     if num == 1:
#         raise NameError('NameError! Вас постигла неудача!')
#     if num == 2:
#         raise BaseException('BaseException! Вас постигла неудача!')
#     if num == 3:
#         raise FloatingPointError('FloatingPointError! Вас постигла неудача!')


def check():
    summ = 0
    with open("out_file.txt", 'r') as file_num:
        for i in file_num:
            summ += int(i)
    return summ

def writting(enter):
    with open("out_file.txt", 'a') as file_num:
        file_num.write(enter + '\n')

def prnt():
    with open("out_file.txt", 'r') as file_num:
        for i in file_num:
            print(i, end="")


while True:
    try:
        enter = int(input("Введите число:"))
        writting(str(enter))
        n = check()
        chance = random.randint(1, 13)
        if chance != 13:
            if n >= 777:
                print("Вы успешно выполнили условие для выхода из порочного цикла!", n)
                print("Содержимое файла out_file.txt:")
                prnt()
                with open('out_file.txt', 'w'):
                    pass
                break

        else:
            raise BaseException("END!")
    except BaseException as a:
        print('Вас постигла неудача!', a)
        print("Содержимое файла out_file.txt:")
        prnt()
        with open('out_file.txt', 'w'):
            pass
        break
    #print(n)

