count = 0
line_count = 0
try:
    with open("people.txt", 'r', encoding='utf8') as fle:
        for i in fle:
            tmp = i.strip('\n')
            line_count += 1
            if len(tmp) < 3:
                print("Ошибка: менее трёх символов в строке {}.".format(line_count))
                count += len(tmp)
            else:
                count += len(tmp)

except FileNotFoundError as exc:
    print("не найден файл", exc)
finally:
    print("Общее количество символов: ", count)
