# TODO здесь писать код
def count_uppercase_lowercase(text):
    uppercase = 0
    lowercase = 0
    nov = "1234567890!?, .-=+"
    for i in range(len(text)):
        if text[i].isupper():
            uppercase += 1
        elif not text[i] in nov:
            lowercase += 1
    return uppercase, lowercase

# Пример использования:
text = input("Введите строку для анализа: ")
uppercase, lowercase = count_uppercase_lowercase(text)
print("Количество заглавных букв:", uppercase)
print("Количество строчных букв:", lowercase)
