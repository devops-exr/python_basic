# TODO здесь писать код
first_st = input("Первая строка: ")
second_st = input("Вторая строка: ")
if first_st == second_st:
    print("Одинаковые строки")
elif len(first_st) != len(second_st):
    print("Разная длинна строк")
else:
    counter = 1
    yes = False
    for i in range(len(first_st)-1):
        second_st = second_st[-1] + second_st[:-1]
        if second_st == first_st:
            yes = True
            print("Первая строка получается из второй со сдвигом", counter)
            break
        else:
            counter += 1
    if not yes:
        print("Первую строку нельзя получить из второй с помощью циклического сдвига.")


