def inall(first, sec, third):
    ftarr = [i for i in first if i in sec]
    secarr = [i for i in ftarr if i in third]
    # tmp = []
    # ftarr = []
    # for i in first:
    #     for j in sec:
    #         if j in third:
    #             tmp.append(j)
    #     if i in tmp:
    #         ftarr.append(i)
    return secarr
def diff(first, sec, third):
    newlist = []
    final = []

    # for sec in third:
    #     continue
    # else:
    #     newlist.append()
    # print(newlist)
    for i in first:
        if i not in sec and i not in third:
            final.append(i)
        else:
            continue

    return final


array_1 = [1, 2, 3, 4]
array_2 = [2, 4]
array_3 = [2, 3]

setarr1, setarr2, setarr3 = set(array_1), set(array_2), set(array_3)
eleminall = setarr1.intersection(setarr2.intersection(array_3))
find = setarr1.difference(setarr2.union(setarr3))
#
print(f'Вывод: \n Задача 1: \n Решение с множествами: {eleminall} \n Решение без множеств: {inall(array_1, array_2, array_3)} \n')
print(f'Вывод: \n Задача 2: \n Решение без множеств: {find} \n Решение без множеств: {diff(array_1, array_2, array_3)} \n')