word = int(input("Введите количество пар слов: "))
synonym_d = dict()

for i in range(word):
    wordinp = input("Введите два слова через '-' ").lower()
    newinp = wordinp.split("-")
    wordinput = {newinp[0]:newinp[1]}
    synonym_d.update(wordinput)

out = ""
while True:
    enter = input("Введите слово: ").lower()
    for key, value in synonym_d.items():
        if enter == key:
            out = synonym_d[key]
            #print("Синоним: ", out)
        elif enter == value:
            out = key
            #print("Синоним: ", out)
    if out == '':
        print('Такого слова в словаре нет.')
    else:
        print("Синоним: ", out)

