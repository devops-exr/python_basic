goods = {
    'Лампа': '12345',
    'Стол': '23456',
    'Диван': '34567',
    'Стул': '45678',
}

store = {
    '12345': [
        {'quantity': 27, 'price': 42},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
        {'quantity': 32, 'price': 520},
    ],
    '34567': [
        {'quantity': 2, 'price': 1200},
        {'quantity': 1, 'price': 1150},
    ],
    '45678': [
        {'quantity': 50, 'price': 100},
        {'quantity': 12, 'price': 95},
        {'quantity': 43, 'price': 97},
    ],
}

#print(store.get(goods["Лампа"]))
# print(type(store['12345']))

for i in goods:
    summ = 0
    count = 0
    for j in store.get(goods[i]):
        summ += j.get("price") * j.get("quantity")
        count += j.get("quantity")
    print(i, "-", count, "штук , стоимость", summ, "рубля")
# if goods.values() == store.keys():
#
# print(goods)