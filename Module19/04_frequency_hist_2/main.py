cnt = dict()
txt = input("Введите текст: ")
for i in txt:
    if i in cnt:
        cnt[i] += 1
    else:
        cnt[i] = 1
print("Оригинальный словарь частот:")
[print(i, ":", cnt[i], end="\n") for i in sorted(cnt.keys())]


invert = dict()
for i, value in sorted(cnt.items()):
    if value in invert:
        invert[value].append(i)
    else:
        invert[value] = [i]
print("Инвертированный словарь частот: \n")
for i, g in sorted(invert.items()):
    print(i, ":", g)
