class Water:
    def __add__(self, other):
        if isinstance(other, Air):
            return Storm()
        elif isinstance(other, Fire):
            return Steam()
        elif isinstance(other, Earth):
            return Dirt()
        else:
            return None


class Fire:
    def __add__(self, other):
        if isinstance(other, Air):
            return Lightning()
        elif isinstance(other, Earth):
            return Lava()
        elif isinstance(other, Water):
            return Steam()
        else:
            return None

class Air:
    def __add__(self, other):
        if isinstance(other, Water):
            return Storm()
        elif isinstance(other, Fire):
            return Lightning()
        elif isinstance(other, Earth):
            return Dust()
        else:
            return None


class Earth:
    def __add__(self, other):
        if isinstance(other, Water):
            return Dirt()
        elif isinstance(other, Air):
            return Dust()
        elif isinstance(other, Fire):
            return Lava()
        else:
            return None


class Storm:
    def __str__(self):
        return 'Storm'


class Steam:
    def __str__(self):
        return 'Steam'


class Dirt:
    def __str__(self):
        return 'Dirt'


class Lightning:
    def __str__(self):
        return 'Lightning'


class Dust:
    def __str__(self):
        return 'Dust'


class Lava:
    def __str__(self):
        return 'Lava'


water = Water()
air = Air()
fire = Fire()
earth = Earth()
result1 = water + fire
result2 = water + air
result3 = water + earth
result4 = air + earth
result5 = fire + earth
print(result1)
print(result2)
print(result3)
print(result4)
print(result5)














# class Water:
#     def __init__(self, name="Water"):
#         self.name = name
#
#     def __add__(self, other):
#         if other:
#             if other.name == "Fire":
#                 return Steam.name
#             elif other.name == "Air":
#                 return Storm.name
#             elif other.name == "Earth":
#                 return Dirt.name
#             else:
#                 return None
#
#
# class Fire:
#     name = "Fire"
#
#     def __add__(self, other):
#         if other:
#             if other.name == "Water":
#                 return Steam.name
#             elif other.name == "Air":
#                 return Lightning.name
#             elif other.name == "Earth":
#                 return Lava.name
#             elif other.name == "Fire":
#                 return Firefire.name
#             else:
#                 return None
#
#
# class Air:
#     name = "Air"
#
#     def __add__(self, other):
#         if other:
#             if other.name == "Water":
#                 return Storm.name
#             elif other.name == "Fire":
#                 return Lightning.name
#             elif other.name == "Earth":
#                 return Dust.name
#             else:
#                 return None
#
#
# class Earth:
#     name = "Earth"
#
#     def __add__(self, other):
#         if other:
#             if other.name == "Water":
#                 return Dirt.name
#             elif other.name == "Air":
#                 return Dust.name
#             elif other.name == "Fire":
#                 return Lava.name
#             else:
#                 return None
#
# class Firefire:
#     name = "DoubleFire"
#
# class Storm:
#     name = "Storm"
#
#
# class Steam:
#     name = "Steam"
#
#
# class Dirt:
#     name = "Dirt"
#
#
# class Lightning:
#     name = "Lightning"
#
#
# class Dust:
#     name = "Dust"
#
#
# class Lava:
#     name = "Lava"
#
#
# water = Water()
# fire = Fire()
# air = Air()
# earth = Earth()
# result1 = water + fire
# result2 = fire + air
# result3 = air + earth
# result4 = air + air
# result5 = fire + fire
# print(result1)
# print(result2)
# print(result3)
# print(result4)
# print(result5)
