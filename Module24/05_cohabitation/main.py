import random


class Person:
    def __init__(self, name, satiety=50, house=True):
        self.name = name
        self.satiety = satiety
        self.house = house

    def info(self):
        print("Имя - {}\nCтепень сытости - {}\nДом - {}\n".format(self.name, self.satiety, self.house))

    def eat(self):
        House.ref_food -= 1
        self.satiety += 1

    def work(self):
        self.satiety -= 1
        House.table_money += 1

    def play(self):
        self.satiety -= 1

    def go_to_market(self):
        House.ref_food += 1
        House.table_money -= 1


class House:
    ref_food = 50
    table_money = 0

    def info(self):
        print("Xолодильник с едой - {}\nTумбочка с деньгами - {}\n".format(self.ref_food, self.table_money))


def select(human, human2):
    for i in range(365):
        print("1. Работать; \
              2. Поесть; \
              3. Играть; \
              4. Пойти в магазин.")
        choice = random.randint(1, 6)
        choice2 = random.randint(1, 6)

        if human.satiety < 0:
            print("{}Game over!".format(human.name))
            break

        elif human.satiety < 20:
            human.eat()

        elif human_house.ref_food < 10:
            human.go_to_market()

        elif human_house.table_money < 50:
            human.work()

        elif choice == 1:
            human.work()

        elif choice == 2:
            human.eat()

        else:
            human.play()

        if human2.satiety < 0:
            print("{}Game over!".format(human2.name))
            break

        elif human2.satiety < 20:
            human2.eat()

        elif human_house.ref_food < 10:
            human2.go_to_market()

        elif human_house.table_money < 50:
            human2.work()

        elif choice == 1:
            human2.work()

        elif choice == 2:
            human2.eat()

        else:
            human2.play()

        human.info()
        human2.info()
        human_house.info()


enter = input("Введите первое имя: ")
enter2 = input("Введите второе имя: ")

human = Person(enter)
human2 = Person(enter2)
human_house = House()
select(human, human2)
