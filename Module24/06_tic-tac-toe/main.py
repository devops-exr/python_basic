class Cell:
    def __init__(self, number):
        self.number = number
        self.typ_cell = ' '
        self.free = True

    # def set_value(self, typ_cell):
    #     if self.typ_cell == ' ':
    #         self.typ_cell = typ_cell
    #         return True
    #     else:
    #         return False


class Board:
    cell = 3
    cells = [Cell(i) for i in range(1, 10)]

    def show_board(self):
        print(f'\t\t\t {self.cells[0].typ_cell} | {self.cells[1].typ_cell} | {self.cells[2].typ_cell} ')
        print("\t\t", '-' * 17)
        print(f'\t\t\t {self.cells[3].typ_cell} | {self.cells[4].typ_cell} | {self.cells[5].typ_cell} ')
        print("\t\t", '-' * 17)
        print(f'\t\t\t {self.cells[6].typ_cell} | {self.cells[7].typ_cell} | {self.cells[8].typ_cell} ')

    def show_board_sec(self):
        print(f'\t\t\t {self.cells[0].number} | {self.cells[1].number} | {self.cells[2].number} ')
        print('-' * 11)
        print(f'\t\t\t {self.cells[3].number} | {self.cells[4].number} | {self.cells[5].number} ')
        print('-' * 11)
        print(f'\t\t\t {self.cells[6].number} | {self.cells[7].number} | {self.cells[8].number} ')


class Player:
    count = 0

    def __init__(self, name, typ_cell, step=0, score=0):
        self.name = name
        self.step = step
        self.score = score
        self.count += 1
        self.typ_cell = typ_cell
        # if self.count == 1:
        #     self.typ_cell = 'X'
        # else:
        #     self.typ_cell = 'O'

    def step_player(self, number):
        board.cells[number - 1].typ_cell = self.typ_cell
        board.cells[number - 1].free = False

    def info_player(self):
        pass


class Game:
    player_1 = Player('Иван', 'X')
    player_2 = Player('Степан', 'O')
    players = [player_1, player_2]

    def end_game(self):
        if ((board.cells[0].typ_cell == board.cells[1].typ_cell == board.cells[2].typ_cell != ' ') or
                (board.cells[3].typ_cell == board.cells[4].typ_cell == board.cells[5].typ_cell != ' ') or
                (board.cells[6].typ_cell == board.cells[7].typ_cell == board.cells[8].typ_cell != ' ') or
                (board.cells[0].typ_cell == board.cells[3].typ_cell == board.cells[6].typ_cell != ' ') or
                (board.cells[1].typ_cell == board.cells[4].typ_cell == board.cells[7].typ_cell != ' ') or
                (board.cells[2].typ_cell == board.cells[5].typ_cell == board.cells[8].typ_cell != ' ') or
                (board.cells[0].typ_cell == board.cells[4].typ_cell == board.cells[8].typ_cell != ' ') or
                (board.cells[2].typ_cell == board.cells[4].typ_cell == board.cells[6].typ_cell != ' ')):

            return True
        else:
            return False


        # for i in self.players:
        #     if ((board.cells[0].typ_cell == board.cells[1].typ_cell == board.cells[2].typ_cell != ' ') or
        #             (board.cells[3].typ_cell == board.cells[4].typ_cell == board.cells[5].typ_cell != ' ') or
        #             (board.cells[6].typ_cell == board.cells[7].typ_cell == board.cells[8].typ_cell != ' ') or
        #             (board.cells[0].typ_cell == board.cells[3].typ_cell == board.cells[6].typ_cell != ' ') or
        #             (board.cells[1].typ_cell == board.cells[4].typ_cell == board.cells[7].typ_cell != ' ') or
        #             (board.cells[2].typ_cell == board.cells[5].typ_cell == board.cells[8].typ_cell != ' ') or
        #             (board.cells[0].typ_cell == board.cells[4].typ_cell == board.cells[8].typ_cell != ' ') or
        #             (board.cells[2].typ_cell == board.cells[4].typ_cell == board.cells[6].typ_cell != ' ')):
        #         print(f'Победил игрок {i.name}!')
        #         break
        #     elif all(cell.typ_cell != 'X' and cell.typ_cell != 'O' for cell in board.cells):
        #         print('Ничья!')

    def one_step_of_game(self, person):
        while True:
            step = int(input("{}, введите номер клетки (1-9):".format(person.name)))
            if 1 <= step <= 9 and board.cells[step - 1].free:
                person.step_player(step)
                break
            else:
                print('Игрок {}, Вы совершили ошибку. Попробуйте еще раз.'.format(person.name))

    def start_game(self):
        count = 0
        while True:
            board.show_board()
            if count % 2 == 0:
                main_game.one_step_of_game(self.player_1)
                if main_game.end_game():
                    print(f'Результат: {self.player_1.name} победил!')
                    self.player_1.score += 1
                    print("Счет игрока {}: {}\nСчет игрока {}: {}".format(self.player_1.name, self.player_1.score,
                                                                          self.player_2.name, self.player_2.score))
                    break
            else:
                main_game.one_step_of_game(self.player_2)
                if main_game.end_game():
                    print(f'Результат: {self.player_2.name} победил!')
                    self.player_2.score += 1
                    print("Счет игрока {}: {}\nСчет игрока {}: {}".format(self.player_1.name, self.player_1.score,
                                                                          self.player_2.name, self.player_2.score))
                    break
            count += 1
            if count == 9:
                print(f'Ничья!')
                print("Счет игрока {}: {}\nСчет игрока {}: {}".format(self.player_1.name, self.player_1.score,
                                                                      self.player_2.name, self.player_2.score))
                break
        board.show_board()

    def main_game_start(self):
        while True:
            enter = input("Начать новую игру? Введите Y(y) или N(n): ")
            if enter.lower() == "y":
                board.cells = [Cell(i) for i in range(1, 10)]
                self.start_game()
            else:
                print("Игра окончена.")
                break


main_game = Game()
board = Board()
main_game.main_game_start()
