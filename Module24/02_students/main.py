class Student:
    def __init__(self, fi, group, perfomance):
        self.fi = fi
        self.group = group
        self.perfomance = perfomance

    def print_info(self):
        return "ФИ: {}; Группа: {}; Успеваемость: {}".format(self.fi, self.group, self.perfomance)

    def counting(self):
        step = 0
        summ = 0
        for j in self.perfomance:
            step += 1
            summ += j
        result = summ / step
        return result


def sort(students):
    res = {}
    for i in students:
        n = i.counting()
        res.update({i.print_info(): n})
    srt = sorted(res.items(), key=lambda st: st[1])
    return srt

def printing(result):
    for i in result:
        print(i, end="\n")


students = [Student("Нивин Олег", "ДН-123", [5, 2, 3, 4, 5]),
            Student("Фролова Ольга", "ГР-212", [2, 2, 3, 4, 5]),
            Student("Олимп Виктория", "ЮР-234", [3, 2, 3, 4, 5]),
            Student("Романов Роман", "ТР-212", [21, 1, 1, 1, 1])]

result = sort(students)
printing(result)
