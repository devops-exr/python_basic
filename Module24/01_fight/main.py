import random
class Warrior:
    count = 0
    def __init__(self, name='', hp=100, kick=20):
        self.name = name
        self.hp = hp
        self.kick = kick

    def fight(self):
        self.hp -= self.kick

def start_fighting(unit, unit2):
    n = True
    while n:
        Warrior.count += 1
        print('-' * 13, "Раунд - ", Warrior.count, '-' * 13)
        if random.randint(1, 2) == 1:
            print("^{}^ совершает удар по ^{}^".format(unit.name, unit2.name))
            unit2.fight()
            print("Соперник: ^{}^, Здоровье: {}".format(unit2.name, unit2.hp))
            if unit2.hp == 0:
                print("Победил: ^{}^".format(unit.name))
                n = False
        else:
            print("^{}^ совершает удар по ^{}^".format(unit2.name, unit.name))
            unit.fight()
            print("Соперник: ^{}^, Здоровье: {}".format(unit.name, unit.hp))
            if unit.hp == 0:
                print("Победил: ^{}^".format(unit2.name))
                n = False


unit = Warrior("Tanos")
unit2 = Warrior("Iron_Man")
start_fighting(unit, unit2)


