import random


class Child:
    def __init__(self, name, age, state_of_calm, hungry):
        self.name = name
        self.age = age
        self.state_of_calm = state_of_calm
        self.hungry = hungry

    def kid_info(self):
        print(
            "Child name: {};\tAge: {};\tState of calm: {};\tHungry: {}".format(self.name, self.age, self.state_of_calm,
                                                                               self.hungry))


class Parent:
    def __init__(self, name, age, lst_children):
        self.name = name
        self.age = age
        self.lst_children = lst_children
        self.lst_kids = []
        for i, v in enumerate(self.lst_children):
            self.lst_kids.append(Child(v, random.randint(2, 8), random.choice([True, False]),
                                       random.choice([True, False])))
            self.lst_kids[i].kid_info()
            if self.age - 16 >= self.lst_kids[i].age:
                print("Возраст родителя {} больше 16 лет\n".format(self.name))
                self.info()
                self.feed_kid(self.lst_kids[i])
                self.calm_down_kid(self.lst_kids[i])
            else:
                print("Возраст родителя {} меньше 16 лет. Возможно, произошла ошибка\n".format(self.name))
                break


    def info(self):
        print("Имя родителя: {}\nВозраст: {}\nДети: {}\n".format(self.name, self.age,
                                                                            [i for i in self.lst_children]))

    def calm_down_kid(self, kid):
        #for i in self.lst_kids:
            if kid.state_of_calm:
                print("Ребенок {} спокоен\n".format(kid.name))

            else:
                print("Успокаиваем ребенка {}\n".format(kid.name))

    def feed_kid(self, kid):
        #for i in self.lst_kids:
            if kid.hungry:
                print("Кормим ребенка {}".format(kid.name))
            else:
                print("Ребенок {} не голоден".format(kid.name))


first = Parent("Max", 23, ["Мария", "Иван", "Андрей"])





























# import random
#
#
# class Child:
#     def __init__(self, name, age, state_of_calm, hungry):
#         self.name = name
#         self.age = age
#         self.state_of_calm = state_of_calm
#         self.hungry = hungry
#
#     def kid_info(self):
#         print(
#             "Child name: {};\tAge: {};\tState of calm: {};\tHungry: {}".format(self.name, self.age, self.state_of_calm,
#                                                                                self.hungry))
#
#
# class Parent:
#     def __init__(self, name, age, lst_children):
#         self.name = name
#         self.age = age
#         self.lst_children = lst_children
#         self.lst_kids = []
#         for i, v in enumerate(self.lst_children):
#             self.lst_kids.append(Child(v, random.randint(2, 8), random.choice([True, False]),
#                                        random.choice([True, False])))
#             self.lst_kids[i].kid_info()
#             if self.age - 16 >= self.lst_kids[i].age:
#                 print("Возраст родителя {} больше 16 лет\n".format(self.name))
#             else:
#                 print("Возраст родителя {} меньше 16 лет. Возможно, произошла ошибка\n".format(self.name))
#                 break
#
#
#
#
#
#     def info(self):
#         print("Имя родителя: {}\nВозраст: {}\nДети: {}\n".format(self.name, self.age,
#                                                                             [i for i in self.lst_children]))
#
#     def calm_down_kid(self):
#         for i in self.lst_kids:
#             if i.state_of_calm:
#                 print("Ребенок {} спокоен".format(i.name))
#
#             else:
#                 print("Успокаиваем ребенка {}".format(i.name))
#
#     def feed_kid(self):
#         for i in self.lst_kids:
#             if i.hungry:
#                 print("Кормим ребенка {}".format(i.name))
#             else:
#                 print("Ребенок {} не голоден".format(i.name))
#
#
# first = Parent("Max", 23, ["Мария", "Иван"])
# first.info()
# first.feed_kid()
# first.calm_down_kid()
#
#
