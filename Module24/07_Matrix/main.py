class Matrix:
    def __init__(self, count, elem, data=None):
        self.count = count
        self.elem = elem
        self.data = data

    def addition(self, matrix):
        result_data = []
        for i in range(self.count):
            row = []
            for j in range(self.elem):
                row.append(self.data[i][j] + matrix.data[i][j])
            #print(i, self.data[0])
            result_data.append(row)
        return result_data

    def subtraction(self, matrix):
        result_data = []
        for i in range(self.count):
            row = []
            for j in range(self.elem):
                row.append(self.data[i][j] - matrix.data[i][j])
            #print(i, self.data[0])
            result_data.append(row)
        return result_data

    def multiply(self, matrix):
        result_data = []
        for i in range(matrix.elem):
            row = [self.data[i][j] * matrix.data[i][j] for j in range(matrix.elem)]
            result_data.append(row)
        return result_data

    def transpose(self):
        result_data = []
        for i in range(self.elem):
            row = []
            for j in range(self.count):
                row.append(self.data[j][i])
            result_data.append(row)
        return result_data

    def printing(self):
        for i in self.data:
            for j in i:
                print(j, end=" ")
            print()


def print_lst(data):
    for i in data:
        for j in i:
            print(j, end=" ")
        print()


# Примеры работы с классом:

# Создание экземпляров класса Matrix
m1 = Matrix(2, 3)
m1.data = [[1, 2, 3], [4, 5, 6]]

m2 = Matrix(2, 3)
m2.data = [[7, 8, 9], [10, 11, 12]]

# Тестирование операций
print("Матрица 1:")
m1.printing()
#print(m1)

print("Матрица 2:")
m2.printing()
#print(m2)

print("Сложение матриц:")
print_lst(m1.addition(m2))

print("Вычитание матриц:")
print_lst(m1.subtraction(m2))

m3 = Matrix(3, 2)
m3.data = [[1, 2], [3, 4], [5, 6]]

print("Умножение матриц:")
print_lst(m1.multiply(m3))

print("Транспонирование матрицы 1:")
print_lst(m1.transpose())
