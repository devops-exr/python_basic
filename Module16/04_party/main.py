guests = ['Петя', 'Ваня', 'Саша', 'Лиза', 'Катя']
# TODO здесь писать код
print("Сейчас на вечеринке 5 человек: ", guests)
while True:
    enter = input("Гость пришёл или ушёл? ")
    if enter == "пришел":
        name = input("Имя гостя: ")
        if len(guests) > 5:
            print("Прости, ", name, " но мест нет. ")
        else:
            guests.append(name)
            print("Привет, ", name, "!")
    elif enter == "ушел":
        name = input("Имя гостя: ")
        if guests.count(name) == 0:
            print("ERROR, такого человека нет на вечеринке")
        else:
            guests.remove(name)
            print("Пока, ", name, "!")
    elif enter == "пора спать":
        break
    else:
        print("Сейчас на вечеринке ", guests)

