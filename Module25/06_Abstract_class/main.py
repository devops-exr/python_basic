import math
from abc import ABC, abstractmethod


class Shape(ABC):
    """
    Базовый класс Shape для всех фигур. Родитель: ABC
    """
    @abstractmethod
    def area(self):
        """
        Абстрактный метод area
        :return:
        """
        pass


class Circle(Shape):
    """
    Класс Circle. Родитель: Shape
    Attributes:
        radius (int) - радиус круга
        pi (float) - число PI
    """
    def __init__(self, radius):
        self.radius = radius
        self.pi = math.pi

    def area(self):
        """
        Метод area. Переопределенный от класса Shape
        Attributes:
            result (float) - результат вычисления
        :return: round(result, 2) (float)
        """
        print(self.pi)
        result = self.pi * (self.radius * 2)
        return round(result, 2)


class Rectangle(Shape):
    """
    Класс Rectangle. Родитель: Shape
    Attributes:
        a (int) - длинна прямоугольника
        b (int) - ширина прямоугольника
    """
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def area(self):
        """
        Метод area. Переопределенный от класса Shape
        Attributes:
            result (int) - результат вычисления
        :return: result (int)
        """
        result = self.a * self.b
        return result


class Triangle(Shape):
    """
    Класс Triangle. Родитель: Shape
    Attributes:
        a (int) - длинна треугольник
        h (int) - высота треугольник
    """
    def __init__(self, a, h):
        self.a = a
        self.h = h

    def area(self):
        """
        Метод area. Переопределенный от класса Shape
        Attributes:
            result (float) - результат вычисления
        :return: result (float)
        """
        result = (self.a * self.h) / 2
        return result


# Примеры работы с классом:
# Создание экземпляров классов
# test = Shape()
circle = Circle(5)
rectangle = Rectangle(4, 6)
triangle = Triangle(3, 8)
# Вычисление площади фигур
circle_area = circle.area()
rectangle_area = rectangle.area()
triangle_area = triangle.area()

# Вывод результатов
print("Площадь круга:", circle_area)
print("Площадь прямоугольника:", rectangle_area)
print("Площадь треугольника:", triangle_area)
