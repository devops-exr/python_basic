class Stack:
    """
    Класс Stack.
    Args:
        lst - пустой список
    """
    def __init__(self):
        self.lst = []

    def __str__(self):
        return "; ".join(self.lst)
        #return str(self.lst)

    def addition(self, elem):
        """
        Метод addition. Добавление значений в список
        Args:
            lst - список
        :param: elem - элемент, который нужно добавить в список
        """

        self.lst.append(elem)

    def remove(self):
        """
        Метод remove. Для удаления элемента со списка
        :arg: result - переменная для храненния результата
        :return: result
        """

        if len(self.lst) == 0:
            return "Anything"
        else:
            result = self.lst.pop()
            return result


class TaskManager:
    """
    Класс TaskManager
    Args:
        dct - словарь
    """

    def __init__(self):
        self.dct = {}

    def __str__(self):
        """
        Метод для представления строкового представления объекта
        Attributes:
            lst - создание пустого списка
        :return: ''.join(lst) (str)
        """
        lst = []
        for i in sorted(self.dct.keys()):
            lst.append(f"{i} - {self.dct[i]}\n")
        return ''.join(lst)

    def new_task(self, task, priority):
        """
        Метод new_task. Создание новой задачи
        :param task: задание
        :param priority: приоритет
        """

        if priority in self.dct.keys():
            self.dct[priority].addition(task)
        else:
            self.dct[priority] = Stack()
            self.dct[priority].addition(task)








manager = TaskManager()
manager.new_task("сделать уборку", 4)
manager.new_task("помыть посуду", 4)
manager.new_task("отдохнуть", 1)
manager.new_task("поесть", 2)
manager.new_task("сдать ДЗ", 2)
print(manager)
# print(TaskManager.__doc__)

# item = Stack()
# item.addition("GLww")
# item.addition("123")
# item.remove()
# print(item)
