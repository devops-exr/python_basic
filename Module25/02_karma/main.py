import random


class KillError(Exception):
    """
    Класс KillError. Родитель: Exception
    """

    def __str__(self):
        return "!__KillError__!"


class DrunkError(Exception):
    """
    Класс DrunkError. Родитель: Exception
    """

    def __str__(self):
        return "!__DrunkError__!"


class CarCrashError(Exception):
    """
    Класс CarCrashError. Родитель: Exception
    """

    def __str__(self):
        return "!__CarCrashError__!"


class GluttonyError(Exception):
    """
    Класс GluttonyError. Родитель: Exception
    """

    def __str__(self):
        return "!__GluttonyError__!"


class DepressionError(Exception):
    """
    Класс DepressionError. Родитель: Exception
    """

    def __str__(self):
        return "!__DepressionError__!"


class Buddist:
    """
    Класс Buddist. Описывающий буддиста
    __const_karma - константа очков кармы

    Arg:
        karma - количество кармы

    """

    __const_karma = 500

    def __init__(self, karma):
        self.__karma = karma

    def one_day(self):
        """
        Метод one_day, которая возвращает количество кармы
        Arg:
            value_karma - случайное количество кармы
            chance - шанс (вероятность 1 к 10 выкинуть одно из исключений)

        :return:value_karma (int)
        """
        value_karma = random.randint(1, 7)
        chance = random.randint(1, 10)
        if chance == 3:
            return random.choice(functions)
        else:
            return value_karma

    def set_karma(self, val):
        """
        Сеттер для установления количества кармы
        :param val: значение кармы
        """

        self.__karma += val

    def get_karma(self):
        """
        Геттер для получения кармы
        :return: __karma - (int)
        """

        return self.__karma

    def get_const(self):
        """
        Геттер для получения константы
        :return: __const_karma - (int)
        """

        return self.__const_karma


person = Buddist(0)
person.get_karma()

functions = [KillError(), DrunkError(), CarCrashError(), GluttonyError(), DepressionError()]
with(open('karma.log', 'w', encoding='utf8') as file):
    errors = 0
    days = 0
    while True:
        days += 1
        if person.get_karma() >= person.get_const():
            print(f"Карма = {person.get_karma()}\nПрошло дней: {days}\nОшибки: {errors}")
            break
        res = person.one_day()
        if res in functions:
            file.write(str(res) + "\n")
            errors += 1
        else:
            person.set_karma(res)




# class KillError(Exception):
#     def __str__(self):
#         return "!__KillError__!"
#
#
# class DrunkError(Exception):
#     def __str__(self):
#         return "!__DrunkError__!"
#
#
# class CarCrashError(Exception):
#     def __str__(self):
#         return "!__CarCrashError__!"
#
#
# class GluttonyError(Exception):
#     def __str__(self):
#         return "!__GluttonyError__!"
#
#
# class DepressionError(Exception):
#     def __str__(self):
#         return "!__DepressionError__!"
#
#
# def one_day():
#     count_karma = random.randint(1, 7)
#     chance = random.randint(1, 10)
#     if chance == 3:
#         return random.choice(functions)
#     else:
#         return count_karma
#
#
# functions = [KillError(), DrunkError(), CarCrashError(), GluttonyError(), DepressionError()]
# with(open('karma.log', 'w', encoding='utf8') as file):
#     CONST = 500
#     karma = 0
#     days = 0
#     while True:
#         days += 1
#         if karma >= CONST:
#             print(f"Karma = {karma}. Прошло дней: {days}")
#             break
#         res = one_day()
#         if res in functions:
#             file.write("\n" + str(res))
#         else:
#             karma += res




