import random


class Hero:
    """
    Класс Hero
    :arg max_hp (int) - начальное значение здоровья
    :arg start_power (int) - начальное значение силы

    Attributes:
        name - имя героя
        __hp - здоровье героя
        __power - сила героя
        __is_alive - живой ли герой

    """
    # Базовый класс, который не подлежит изменению
    # У каждого наследника будут атрибуты:
    # - Имя
    # - Здоровье
    # - Сила
    # - Жив ли объект
    # Каждый наследник будет уметь:
    # - Атаковать
    # - Получать урон
    # - Выбирать действие для выполнения
    # - Описывать своё состояние

    max_hp = 150
    start_power = 10

    def __init__(self, name):
        self.name = name
        self.__hp = self.max_hp
        self.__power = self.start_power
        self.__is_alive = True

    def get_hp(self):
        """
        Геттер для получения значения здоровья
        :return: self.__hp
        """
        return self.__hp

    def set_hp(self, new_value):
        """
        Сеттер для установки значения здоровья
        """
        self.__hp = max(new_value, 0)

    def get_power(self):
        """
        Геттер для получения значения силы
        :return: self.__power
        """
        return self.__power

    def set_power(self, new_power):
        """
        Сеттер для установки значения силы
        :return: self.__power
        """
        self.__power = new_power

    def is_alive(self):
        """
        Геттер для получения значения жизни монстра
        :return: self.__is_alive
        """
        return self.__is_alive

    # Все наследники должны будут переопределять каждый метод базового класса (кроме геттеров/сеттеров)
    # Переопределенные методы должны вызывать методы базового класса (при помощи super).
    # Методы attack и __str__ базового класса можно не вызывать (т.к. в них нету кода).
    # Они нужны исключительно для наглядности.
    # Метод make_a_move базового класса могут вызывать только герои, не монстры.
    def attack(self, target):
        """
        Метод attack - атака
        :param target: монстр
        :raise: NotImplementedError("Вы забыли переопределить метод Attack!")
        """
        # Каждый наследник будет наносить урон согласно правилам своего класса
        raise NotImplementedError("Вы забыли переопределить метод Attack!")

    def take_damage(self, damage):
        """
        Метод take_damage - получение урона
        :arg damage - урон
        :return: self.__is_alive
        """
        # Каждый наследник будет получать урон согласно правилам своего класса
        # При этом у всех наследников есть общая логика, которая определяет жив ли объект.
        print("\t", self.name, "Получил удар с силой равной = ", round(damage), ". Осталось здоровья - ",
              round(self.get_hp()))
        # Дополнительные принты помогут вам внимательнее следить за боем и изменять стратегию, чтобы улучшить выживаемость героев
        if self.get_hp() <= 0:
            self.__is_alive = False

    def make_a_move(self, friends, enemies):
        """
        Метод make_a_move - сделать действие
        :param friends: герои
        :param enemies: монстры
        """
        # С каждым днём герои становятся всё сильнее.
        self.set_power(self.get_power() + 0.1)

    def __str__(self):
        return f"Name: {self.name}. HP: {self.get_hp()}. Power: {self.get_power()}"
        # Каждый наследник должен выводить информацию о своём состоянии, чтобы вы могли отслеживать ход сражения
        #raise NotImplementedError("Вы забыли переопределить метод __str__!")


#*------------------------------------
class Healer(Hero):
    """
    Класс Healer. Родитель Hero

    :arg max_hp (int) - начальное значение здоровья
    :arg start_power (int) - начальное значение силы

    Attributes:
        magic_pwr - магическая сила
    """
    def __init__(self, name):
        super().__init__(name)
        self.magic_pwr = self.get_power() * 3

    # def set_power(self, new_power):
    #     self.__power = new_power

    def attack(self, target):
        """
        Метод attack - атака
        :param target: монстр
        """
        target.take_damage(self.get_power() / 2)

    def take_damage(self, damage):
        """
        Метод take_damage - получение урона
        :param damage: значение силы
        """
        self.set_hp(self.get_hp() - (1.2 * damage))
        super().take_damage(damage)

    def heal(self, unit):
        """
        Метод heal - лечение
        :param unit: передается значение героя
        """

        unit.set_hp(unit.get_hp() + self.magic_pwr)
        print(f"Healing hero: {unit.name}. HP: {unit.get_hp()}")

    def make_a_move(self, friends, enemies):
        """
        Метод make_a_move - сделать действие

        :param friends: герои
        :param enemies: монстры
        """
        print("Character move -->> " + self.__str__())
        tmp = []
        for i in range(len(friends)):
            tmp.append(friends[i].get_hp())
        minimal = min(tmp)
        for i in friends:
            if i.get_hp() <= minimal:
                self.heal(i)
                break
        else:
            self.attack(random.choice(enemies))
        super().make_a_move(friends, enemies)


# Целитель:
# Атрибуты:
# - магическая сила - равна значению НАЧАЛЬНОГО показателя силы умноженному на 3 (self.__power * 3)
# Методы:
# - атака - может атаковать врага, но атакует только в половину силы self.__power
# - получение урона - т.к. защита целителя слаба - он получает на 20% больше урона (1.2 * damage)
# - исцеление - увеличивает здоровье цели на величину равную своей магической силе
# - выбор действия - получает на вход всех союзников и всех врагов и на основе своей стратегии выполняет ОДНО из действий (атака,
# исцеление) на выбранную им цель


class Tank(Hero):
    """
    Класс Healer. Родитель Hero

    :arg max_hp (int) - начальное значение здоровья
    :arg start_power (int) - начальное значение силы

    Attributes:
        defense - защита
        active_shield (bool)- поднят ли щит
    """
    def __init__(self, name):
        super().__init__(name)
        self.defense = 1
        self.active_shield = True

    def attack(self, target):
        """
         Метод attack - атака
         :param target: монстр
        """
        target.take_damage(self.get_power() / 2)

    def take_damage(self, damage):
        """
        Метод take_damage - получение урона
        :param damage: значение силы
        """
        self.set_hp(self.get_hp() - (damage / self.defense))
        super().take_damage(damage)

    def up_shield(self):
        """
        Метод up_shield - поднятие щита

        """
        if not self.active_shield:
            self.active_shield = True
            self.defense *= 2
            self.set_power(self.get_power() / 2)

    def down_shield(self):
        """
        Метод down_shield - опускания щита

        """
        if self.active_shield:
            self.active_shield = False
            self.defense /= 2
            self.set_power(self.get_power() * 2)

    def make_a_move(self, friends, enemies):
        """
        Метод make_a_move - сделать действие

        :param friends: список героев
        :param enemies: список монстров
        """
        print("Character move -->> " + self.__str__() + f". Shield: {self.active_shield}")
        for i in enemies:
            if self.active_shield:
                self.attack(random.choice(enemies))
                break
        else:
            self.up_shield()
        super().make_a_move(friends, enemies)


# Танк:
# Атрибуты:
# - показатель защиты - изначально равен 1, может увеличиваться и уменьшаться
# - поднят ли щит - танк может поднимать щит, этот атрибут должен показывать поднят ли щит в данный момент
# Методы:
# - атака - атакует, но т.к. доспехи очень тяжелые - наносит половину урона (self.__power)
# - получение урона - весь входящий урон делится на показатель защиты (damage/self.defense) и только потом отнимается от здоровья
# - поднять щит - если щит не поднят - поднимает щит. Это увеличивает показатель брони в 2 раза, но уменьшает показатель силы в 2 раза.
# - опустить щит - если щит поднят - опускает щит. Это уменьшает показатель брони в 2 раза, но увеличивает показатель силы в 2 раза.
# - выбор действия - получает на вход всех союзников и всех врагов и на основе своей стратегии выполняет ОДНО из действий (атака,
# поднять щит/опустить щит) на выбранную им цель


class Attacker(Hero):
    """
    Класс Healer. Родитель Hero

    :arg max_hp (int) - начальное значение здоровья
    :arg start_power (int) - начальное значение силы

    Attributes:
        power_multiply - умножение силы
    """
    def __init__(self, name):
        super().__init__(name)
        self.power_multiply = 2

    def attack(self, target):
        """
         Метод attack - атака
         :param target: монстр
        """
        target.take_damage(self.get_power() * self.power_multiply)
        self.power_down()

    def take_damage(self, damage):
        """
        Метод take_damage - получение урона
        :param damage: значение силы
        """
        self.set_hp(self.get_hp() - (damage * (self.power_multiply / 2)))
        super().take_damage(damage)

    def power_up(self):
        """
        Метод power_up - повышение силы
        """
        self.power_multiply *= 2

    def power_down(self):
        """
        Метод power_down - снижение силы

        """
        self.power_multiply /= 2

    def make_a_move(self, friends, enemies):
        """
        Метод make_a_move - сделать действие

        :param friends: список героев
        :param enemies: список монстров
        """
        tmp = []
        for i in range(len(enemies)):
            tmp.append(enemies[i].get_hp())
        minimal = min(tmp)
        for i in enemies:
            if i.get_hp() <= minimal and self.power_multiply > 10:
                self.attack(i)
                break
        else:
            self.power_up()
        super().make_a_move(friends, enemies)




# Убийца:
# Атрибуты:
# - коэффициент усиления урона (входящего и исходящего)
# Методы:
# - атака - наносит урон равный показателю силы (self.__power) умноженному на коэффициент усиления урона (self.power_multiply)
# после нанесения урона - вызывается метод ослабления power_down.
# - получение урона - получает урон равный входящему урона умноженному на половину коэффициента усиления урона - damage * (
# self.power_multiply / 2)
# - усиление (power_up) - увеличивает коэффициента усиления урона в 2 раза
# - ослабление (power_down) - уменьшает коэффициента усиления урона в 2 раза
# - выбор действия - получает на вход всех союзников и всех врагов и на основе своей стратегии выполняет ОДНО из действий (атака,
# усиление, ослабление) на выбранную им цель
