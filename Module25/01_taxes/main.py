class Property:
    """
    Базовый класс описывающий имущество налогоплательщиков
    Args:
        worth (int) - стоимость
    """

    def __init__(self, worth):
        self.worth = worth

    def tax_calc(self):
        pass


class Apartment(Property):
    """
    Класс Apartment(квартира). Родитель: Property
    Args:
        worth (int) - стоимость
    """

    def tax_calc(self):
        """
        Mетод расчёта налога
        Налог на квартиру вычисляется как 1/1000 её стоимости
        Args:
            counting (int) - налог на квартиру
            self.worth (int) - стоимость

        :return: counting
        """

        counting = self.worth / 1000
        return counting


class Car(Property):
    """
    Класс Car(машина). Родитель: Property
    Args:
        worth (int) - стоимость
    """

    def tax_calc(self):
        """
        Mетод расчёта налога
        Налог на машину вычисляется как 1/200 её стоимости
        Args:
            counting (int) - налог на машину
            self.worth (int) - стоимость

        :return: counting
        """

        counting = self.worth / 200
        return counting


class CountryHouse(Property):
    """
    Класс CountryHouse(дача). Родитель: Property
    Args:
        worth (int) - стоимость
    """

    def tax_calc(self):
        """
        Mетод расчёта налога
        Налог на дачу вычисляется как 1/500 её стоимости
        Args:
            counting (int) - налог на машину
            self.worth (int) - стоимость

        :return: counting
        """

        counting = self.worth / 500
        return counting


def count_all(mney, item):
    """
    Функция подсчета - запрашивает у пользователя количество его денег и стоимость имущества, а
    затем выдаёт налог на соответствующее имущество и показывает, сколько денег ему не хватает (если это так)
    Args:
        apart = экземпляр класса
        mney (int) - вводимое пользоватилем количество денег
        item (int) - переменная для указания имущества
        enter_worth (int) - вводимое пользоватилем количество денег
    :return: mney
    """

    print("Counting...")
    apart = item(enter_worth)
    mney -= apart.tax_calc()
    print(f"Налог составил: {apart.tax_calc()}")
    print(f"Ваши деньги с учетом высчетов: {mney} руб.")
    return mney


enter_money = int(input("Введите количество денег: "))
while True:
    enter = int(input("1 - квартира; \n"
                      "2 - машина; \n"
                      "3 - дача. \n"
                      "Сделайте выбор: "))
    if enter == 1:
        enter_worth = int(input("Введите стоимость имущества (квартира): "))
        res = count_all(enter_money, Apartment)
        enter_money = res

    elif enter == 2:
        enter_worth = int(input("Введите стоимость имущества (машина): "))
        res = count_all(enter_money, Car)
        enter_money = res

    elif enter == 3:
        enter_worth = int(input("Введите стоимость имущества (дача): "))
        res = count_all(enter_money, CountryHouse)
        enter_money = res
    else:
        if enter_money < 0:
            print(f"Вам не хватает: {enter_money * (-1)} руб.")
        else:
            print(f"Ваши деньги с учетом высчетов: {enter_money} руб.")
        break

#second_version

# class Property:
#     def __init__(self, worth, money):
#         self.worth = worth
#         self.money = money
#
#     def tax_calc(self):
#         pass
#
#
# class Apartment(Property):
#     def __init__(self, worth, money):
#         super().__init__(worth, money)
#
#     def tax_calc(self):
#         print(f"Налог составил: {self.worth / 1000}")
#         return self.money - (self.worth / 1000)
#
#
# class Car(Property):
#     def __init__(self, worth, money):
#         super().__init__(worth, money)
#
#     def tax_calc(self):
#         print(f"Налог составил: {self.worth / 200}")
#         return self.money - (self.worth / 200)
#
#
# class CountryHouse(Property):
#     def __init__(self, worth, money):
#         super().__init__(worth, money)
#
#     def tax_calc(self):
#         print(f"Налог составил: {self.worth / 500}")
#         return self.money - (self.worth / 500)
#
#
# enter_money = int(input("Введите количество денег: "))
# while True:
#     enter = int(input("1 - квартира; \n"
#                       "2 - машина; \n"
#                       "3 - местонахождение квартиры. \n"
#                       "Сделайте выбор: "))
#     if enter == 1:
#         enter_worth = int(input("Введите стоимость имущества (квартира): "))
#         apart = Apartment(enter_worth, enter_money)
#         enter_money = apart.tax_calc()
#         print(f"Ваши деньги с учетом высчетов: {enter_money} руб.")
#
#     elif enter == 2:
#         enter_worth = int(input("Введите стоимость имущества (машина): "))
#         car = Car(enter_worth, enter_money)
#         enter_money = car.tax_calc()
#         print(f"Ваши деньги с учетом высчетов: {enter_money} руб.")
#
#     elif enter == 3:
#         enter_worth = int(input("Введите стоимость имущества (дача): "))
#         country_house = CountryHouse(enter_worth, enter_money)
#         enter_money = country_house.tax_calc()
#         print(f"Ваши деньги с учетом высчетов: {enter_money} руб.")
#     else:
#         if enter_money < 0:
#             print(f"Вам не хватает: {enter_money * (-1)} руб.")
#         else:
#             print(f"Ваши деньги с учетом высчетов: {enter_money} руб.")
#         break
#
