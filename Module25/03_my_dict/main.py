class MyDict(dict):
    """
    Класс MyDict. Родитель: dict
    Который будет вести себя точно так же, как и обычный словарь
    """

    def get(self, key, default=0):
        """
        Геттер для получения результата поиска
        :param key: значение, которое нужно найти в словаре
        :param default: значение, которое вернет геттер, если не найдет удовлетворяющее значение key
        :return: super().get(key, default)
        """

        return super().get(key, default)


diction = {1: "Hi", 2: "Hello", 3: "What`s up?", 4: "Good"}
dictionary = MyDict(diction)
print(dictionary.get(12))
