# TODO здесь писать код
class LRUCache:
    """
    Клас LRUCachе, где хранятся элементы добавленные пользователем
    """
    def __init__(self, capacity: int):
        self.capacity = capacity
        self.__cache = []

    @property
    def cache(self):
        """
        Метод получения самого старого элемента в кэше
        :return: self.__cache[0]
        """
        return self.__cache[0]

    @cache.setter
    def cache(self, value):
        """
        Метод добавления в кэш элементов
        :param value:
        """
        if len(self.__cache) > 0:
            for i in self.__cache:
                if i[0] == value[0]:
                    del self.__cache[i]
        self.__cache.append(value)
        if len(self.__cache) > self.capacity:
            del self.__cache[0]

    def print_cache(self):
        """
        Вывод содержимого LRU Cache
        :return: str
        """
        print('LRU Cache:')
        for elem in self.__cache:
            print(f'{elem[0]} : {elem[1]}')

    def get(self, value):
        """
        Метод получения элемента по запросу
        :param value:
        :return: str
        """
        for i in self.__cache:
            if i[0] == value:
                return i[1]
        else:
            return "Error"


# Создаем экземпляр класса LRU Cache с capacity = 3

cache = LRUCache(3)

# Добавляем элементы в кэш
cache.cache = ("key1", "value1")
cache.cache = ("key2", "value2")
cache.cache = ("key3", "value3")

# # Выводим текущий кэш
cache.print_cache()  # key1 : value1, key2 : value2, key3 : value3

# Получаем значение по ключу
print(cache.get("key2"))  # value2

# Добавляем новый элемент, превышающий лимит capacity
cache.cache = ("key4", "value4")

# Выводим обновленный кэш
cache.print_cache()  # key2 : value2, key3 : value3, key4 : value4
