class MyMath:
    PI = 3.1415926535

    @classmethod
    def circle_len(cls, radius):
        """
        Метод вычисление длины окружности
        :param radius:
        :param cls.PI - число PI
        :return:result
        """
        result = cls.PI * (radius * 2)
        return result

    @classmethod
    def circle_sq(cls, radius):
        """
        Метод вычисление площади окружности
        :param radius:
        :param cls.PI - число PI
        :return:result
        """
        result = cls.PI * (radius ** 2)
        return result

    @classmethod
    def calc_vol_cube(cls, a):
        """
        Метод вычисление объёма куба
        :param a: сторона куба
        :param cls.PI - число PI
        :return:result
        """
        result = a ** 3
        return result

    @classmethod
    def calc_surface_area_sph(cls, radius):
        """
        Метод вычисление площади поверхности сферы
        :param radius:
        :param cls.PI - число PI
        :return:result
        """
        result = 4 * cls.PI * (radius ** 2)
        return result



