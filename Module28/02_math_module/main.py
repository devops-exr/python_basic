from mymath import MyMath
res_1 = MyMath.circle_len(radius=5)
res_2 = MyMath.circle_sq(radius=6)
res_3 = MyMath.calc_vol_cube(a=7)
res_4 = MyMath.calc_surface_area_sph(radius=8)
print(res_1)
print(res_2)
print(res_3)
print(res_4)
