class File:
    """
    Mодернизированная версия контекст-менеджера File
    """

    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode
        self.file = None

    def __enter__(self):
        """
        Специальный метод __enter__
        :return: self.file
        """
        try:
            self.file = open(self.filename, self.mode, encoding='utf8')
        except FileNotFoundError:
            self.file = open(self.filename, 'w', encoding='utf8')
        return self.file

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Специальный метод __exit__
        :param exc_type:
        :param exc_val:
        :param exc_tb:
        :return: True
        """
        self.file.close()
        if exc_type is FileNotFoundError or FileExistsError:
            print("Файл закрыт")
            return True

        return True


with File("01_work_file2.txt", "r") as file:
    test_file = file.read()
