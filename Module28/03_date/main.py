class Date:
    @classmethod
    def from_string(cls, string_date):
        """
        Метод from_string. Проверяет числа даты на корректность
        :param string_date:
        :return:str
        """
        if cls.is_date_valid(string_date):
            date_val = string_date.split("-")
            return f"День: {date_val[0]}    Месяц: {date_val[1]}    Год: {date_val[2]}"

    @classmethod
    def is_date_valid(cls, string_date):
        """
        Метод конвертирующий строку даты в объект класса Date,
        состоящий из соответствующих числовых значений дня, месяца и года
        :param string_date:
        :return: bool / str
        """
        try:
            date_val = string_date.split("-")
            if 31 >= int(date_val[0]) >= 1:
                if 12 >= int(date_val[1]) >= 1:
                    if 2200 >= int(date_val[2]) >= 1900:
                        #result = date_val[0], date_val[1], date_val[2]
                        return True
                    else:
                        return "Год не соответствует требованиям"
                else:
                    return "Месяц не соответствует требованиям"
            else:
                return "Дата не соответствует требованиям"
        except ValueError:
            return "ValueError in date"


date = Date.from_string('10-12-2077')
print(date)
print(Date.is_date_valid('10-12-2077'))
print(Date.is_date_valid('40-12-2077'))
print(Date.is_date_valid('30-13-2077'))
print(Date.is_date_valid('10-12-2212'))
