numbers = []
new_list = []
N = int(input("Введите количество чисел в списке: "))
for i in range(N):
    print("Введите ", i + 1," элемент списка: ", end=" ")
    el = int(input())
    numbers.append(el)

k = int(input("Сдвиг: "))
step = len(numbers) - k

for i in range(len(numbers)):
    new_list.append(numbers[-step+i])

print("Изначальный список: ", numbers)
print("Сдвинутый список: ", new_list)