def search(site, key, level):
    if level == 0:
        if key in site:
            return site[key]
    for i in site.values():
        if isinstance(i, dict):
            res = search(i, key, level-1)
            if res:
                break
    else:
        return None
    return res


site = {
    'html': {
        'head': {
            'title': 'Мой сайт'
        },
        'body': {
            'h2': 'Здесь будет мой заголовок',
            'div': 'Тут, наверное, какой-то блок',
            'p': 'А вот здесь новый абзац'
        }
    }
}
key = input("Введите искомый ключ: ")
depth = input("Использовать глубину: (Y/N): ")
if depth.lower() == "y":
    level = int(input("Введите максимальную глубину:"))
else:
    level = 0

value = search(site, key, level)
if value:
    print(value)
else:
    print("NONE!")




