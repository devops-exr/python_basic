#Видел решение со срезами, но не сильно разобрался

def func(*args, res=[]):
    for i in args:
        if isinstance(i, int):
            res.append(i)
        elif isinstance(i, list):
            func(*i)

    return res

    # def two(tmp):
    #     for i in tmp:
    #         if isinstance(i, list):
    #             two(i)
    #         elif isinstance(i, int):
    #             print(i, end=", ")
    #         else:
    #             print(i, end=", ")
    #
    # for i, v in enumerate(*args):
    #     if isinstance(v, int):
    #         print(v, end=", ")
    #     elif isinstance(v, list):
    #         two(v)


nice_list = [1, 2, [3, 4], [[5, 6, 7], [8, 9, 10]], [[11, 12, 13], [14, 15], [16, 17, 18]]]
print(func(nice_list))
