def again(nums):
    low = []
    high = []
    mid = []
    if len(nums) > 1:
        for i in nums:  # разбил на три части
            if i < nums[-1]:
                low.append(i)  # список с эл меньше number
            elif i > nums[-1]:
                high.append(i)  # список с эл больше number
            else:
                mid.append(i)  # список с эл равно number
        nums = again(low) + mid + again(high)

    return nums


numbers = [4, 9, 2, 7, 5]
print(again(numbers))



# def again(nums):
#     if len(nums) > 1:
#         number = nums[-1]
#
#         low = [i for i in nums if i < number]
#         high = [i for i in nums if i > number]
#         mid = [i for i in nums if i == number]
#         nums = again(low) + mid + again(high)
#
#     return nums