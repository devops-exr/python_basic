def summ(*args):
    result = 0
    for i in args:
        if isinstance(i, (list, set, tuple)):
            result += summ(*i)
        elif isinstance(i, (int, float)):
            result += i
        elif isinstance(i, dict):
            for j in i.values():
                if isinstance(j, str):
                    continue
                else:
                    result += j
            else:
                result += summ(*i)

        #result += i
    return result
print("Ответ в консоли: ", summ([[1, 2, [3]], [1], 3]))
print("Ответ в консоли: ", summ(1, 2, 3, 4, 5))
print(summ([{'a':"1"}, 1, 2, [[3]], {8}, (3, 2), {'a':5}]))
