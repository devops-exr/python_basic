students = {
    1: {
        'name': 'Bob',
        'surname': 'Vazovski',
        'age': 23,
        'interests': ['biology, swimming']
    },
    2: {
        'name': 'Rob',
        'surname': 'Stepanov',
        'age': 24,
        'interests': ['math', 'computer games', 'running']
    },
    3: {
        'name': 'Alexander',
        'surname': 'Krug',
        'age': 22,
        'interests': ['languages', 'health food']
    }
}
def function(students):
    id_age = [(i, students[i]["age"]) for i in students]

    lst = {j for i in students for j in students[i]["interests"]}

    cnt = 0
    for i in students:
        for k in students[i]["surname"]:
            cnt += 1

    print("Список пар «ID студента — возраст»: ", id_age)
    print("Полный список интересов всех студентов: ", lst)
    print("Общая длина всех фамилий студентов: ", cnt)

function(students)



# def f(dict):
#     lst = []
#     string = ''
#     for i in dict:
#         lst += (dict[i]['interests'])
#         string += dict[i]['surname']
#     cnt = 0
#     for s in string:
#         cnt += 1
#     # return lst, cnt
#
#
# pairs = []
# for i in students:
#     pairs += (i, students[i]['age'])
#
#
# my_lst = f(students)[0]
# l = f(students)[1]
# print(my_lst, l)



# print("Полный список интересов всех студентов: ", my_lst)
# print("Общая длина всех фамилий студентов: ", l)

