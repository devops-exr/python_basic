def crypto(func):
    return [val for i, val in enumerate(func) if i >= 2 and is_prime(i)]
def is_prime(numb):
    count = 0
    for i in range(2, numb + 1):
        if numb % i == 0:
            count += 1
    if count <= 1:
        return True
    else:
        return False


numbers = (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
numbers3 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
numbers4 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

print(crypto(numbers))
print(crypto('О Дивный Новый мир!'))
print(crypto(numbers3))
print(crypto(numbers4))

# is_prime(numbers2)
# is_prime(numbers3)
# is_prime(numbers4)