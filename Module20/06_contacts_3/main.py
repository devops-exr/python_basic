contacts = {('Иван','Сидоров'):123,
            ('Ивана','Сидоров'):123}
while True:
    print("Введите номер действия: \n",
          "1. Добавить контакт;\n",
          "2. Найти человека;\n",
          "3. Показать контакты.\n",
          "4. Остановка!")

    enter = int(input("Ваш выбор: "))
    if enter == 1:
        name, surname = input("Введите имя и фамилию нового контакта (через пробел):").split(" ")
        cade = (name, surname)
        if cade in contacts.keys():
            print("Контакт с таким именем и фамилией уже существует. "
                 "Внести изменения в номер телефона? 1-да, 2-нет: ")
            choice = int(input("Введите: "))
            if choice == 1:
                tel_numb = int(input("Введите номер телефона: "))
                contacts.update({cade: tel_numb})
                print("Tекущий словарь контактов: ", contacts)
            else:
                print("Возврат в меню!")

        else:
            tel_numb = int(input("Введите номер телефона: "))
            contacts.update({cade: tel_numb})
            print("Tекущий словарь контактов: ", contacts)


    elif enter == 2:
        count = 0
        search = input("Введите фамилию для поиска: ")
        for i, val in contacts.items():
            if search.lower() == i[1].lower():
                count += 1
                print("Контакт", count, ":", i[0], i[1], val)
        if count == 0:
            print("Даного контакта {0} не существует".format(search))
        print("Результатов: ", count)

    elif enter == 3:
        print(contacts)

    else:
        break

