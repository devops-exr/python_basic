def shortest_sequence_range(string, tpl):
    return min(len(string), len(tpl))


line = 'abcdu'
nums = (10, 20, 30, 40)
new = ((line[i], nums[i]) for i in range(shortest_sequence_range(line, nums)))
for i in new:
    print(i)
print(new)
print(zip(line, nums))


# line = 'abcdu'
# nums = (10, 20, 30, 40)
# # if isinstance(nums, type(nums)):
# #     nums = tuple(nums)
# print(zip(line, nums))
# tmp = int(min(len(line), len(nums)))
# new = [(line[i], nums[i]) for i in range(tmp)]
# for i in new:
#     print(i)


