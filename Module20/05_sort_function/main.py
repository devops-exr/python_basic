def func():
    nums = (124, 2, 5, 1, 0, 3, 24, 56, 22, 11, 6, 8, 9)
    cnt = False
    for i in nums:
        if isinstance(i, float):
            cnt = True
    if cnt:
        print(tuple(nums))
        return nums
    else:
        print(sorted(nums))
func()