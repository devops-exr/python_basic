import random
def func1(rand_nums):
    newlist = []
    tmp = []
    for i, val in enumerate(rand_nums, 1):
        tmp.append(val)
        if i % 2 == 0:
            newlist.append(tuple(tmp))
            tmp = []
    print(newlist)
def func2(rand_nums):
    newlist2 = list()
    for i in range(1, len(rand_nums), 2):
        ttt = list()
        ttt.append(rand_nums[i - 1])
        ttt.append(rand_nums[i])
        ttt = tuple(ttt)
        newlist2.append(ttt)
    print(newlist2)
rand_nums = [random.randint(0,10) for i in range(10)]
result = [(rand_nums[i], rand_nums[i+1]) for i in range(0, len(rand_nums), 2) if i != len(rand_nums)]
func1(rand_nums)
func2(rand_nums)
print(result)
