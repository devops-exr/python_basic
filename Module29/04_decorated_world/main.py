import functools
import time
from datetime import datetime
from typing import Callable, Any, Dict, Optional

def decorator_with_args_for_any_decorator(decorator_args) -> Callable:
    """
    Декоратор, который декорирует другой декоратор и дает возможность принимать произвольные аргументы
    :param decorator_args:
    :return:
    """
    def wrapper(*args, **kwargs):
        return decorator_args(*args, **kwargs)

    return wrapper


@decorator_with_args_for_any_decorator
def decorated_decorator(func: Callable, *args, **kwargs) -> Callable:
    def decorator(func):
        def wrapper(*args, **kwargs) -> Callable:
            return func(*args, **kwargs)

        return wrapper

    return decorator


@decorated_decorator(100, 'рублей', 200, 'друзей')
def decorated_function(text: str, num: int) -> None:
    print("Привет", text, num)


decorated_function("Юзер", 101)
