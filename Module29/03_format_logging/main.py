import functools
import time
from datetime import datetime
from typing import Callable, Any, Dict, Optional


def timer(cls, func, format_date):
    """
    Функция, которая выводит дату и время запуска
    :param cls:
    :param func:
    :param format_date:
    :return:
    """
    @functools.wraps(cls)
    def wrapper(*args, **kwargs):
        #instance = cls(*args, **kwargs)
        f_date = format_date
        for i in f_date:
            if i.isalpha():
                f_date = f_date.replace(i, '%' + i)
        print(f'Запускается {cls.__name__}.{func.__name__} Дата и время запуска: {datetime.strftime(datetime.now(), f_date)}')
        start = time.time()
        result = func(*args, **kwargs)
        end = time.time()
        print(f'Завершение {cls.__name__}.{func.__name__}, время работы = {round(end - start, 3)} сек.')
        return result

    return wrapper



def log_methods(decorator_date):
    """
    Декоратор класса. Получает другой декоратор и применяет к методам класса
    :param decorator_date:
    :return:
    """
    @functools.wraps(decorator_date)
    def decorate(cls):
        for i in dir(cls):
            if i.startswith("__") is False:
                cur_method = getattr(cls, i)
                decor_meth = timer(cls, cur_method, decorator_date)
                setattr(cls, i, decor_meth)
        return cls
    return decorate


@log_methods("b d Y — H:M:S")
class A:
    def test_sum_1(self) -> int:
        print('Тут метод test sum 1')
        number = 100
        result = 0
        for _ in range(number + 1):
            result += sum([i_num ** 2 for i_num in range(10000)])

        return result

@log_methods("b d Y - H:M:S")
class B(A):
    def test_sum_1(self):
        super().test_sum_1()
        print("Наследник test sum 1")


    def test_sum_2(self):
        print("Тут метод test sum 2")
        number = 200
        result = 0
        for _ in range(number + 1):
            result += sum([i_num ** 2 for i_num in range(10000)])

        return result

my_obj = B()
my_obj.test_sum_1()
my_obj.test_sum_2()