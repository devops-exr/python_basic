from typing import Callable, Any, Dict, Optional
app = {}


def callback(path: str) -> Callable:
    """
    Функция обратного вызова
    :param path: str
    """
    def callback_2(func: Any) -> Callable:
        app[path] = func

        def wrapper(*args, **kwargs) -> Any:
            return func()
        return wrapper
    return callback_2


@callback('//')
def example():
    print('Пример функции, которая возвращает ответ сервера')
    return 'OK'


route = app.get('//')
if route:
    response = route()
    print('Ответ:', response)
else:
    print('Такого пути нет')


