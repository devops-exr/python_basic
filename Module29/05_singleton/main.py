import functools


def singleton(cls):
    """
    Декоратор превращающий клас в singleton
    :param cls:
    :return:
    """
    # def wrapper(*args, **kwargs):
    #     return cls
    # return wrapper
    @functools.wraps(cls)
    def wrapper(*args, **kwargs):
        if not wrapper.cache:
            wrapper.cache = cls(*args, **kwargs)
        return wrapper.cache

    wrapper.cache = None
    return wrapper


@singleton
class Example:
    pass

my_obj = Example()
my_another_obj = Example()
#my_another_obj2 = Example()

print(id(my_obj))
print(id(my_another_obj))
#print(id(my_another_obj2))
print(my_obj is my_another_obj)