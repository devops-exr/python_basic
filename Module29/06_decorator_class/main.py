import time
import functools
from datetime import datetime
from typing import Callable, Any, Dict, Optional


class LoggerDecorator:
    """
    Класс декоратор, который логирует аргументы, результаты и время выполнения функции
    """

    def __init__(self, func: Callable) -> None:
        self.func = func

    def __call__(self, *args, **kwargs) -> None:
        start_time = time.time()  # Записываем время до вычислений
        res = self.func(*args, **kwargs)
        end_time = time.time()  # Затем записываем время после вычислений
        execution_time = end_time - start_time  # Их разница будет временем выполнения кода вычислений
        print(f"Вызов функции {self.func.__name__}")
        print(f"Аргументы: {args, kwargs}")
        print(f"Результат: {res}")
        print(f"Время выполнения: {execution_time}")
        #return res


@LoggerDecorator
def complex_algorithm(arg1, arg2) :
    # Здесь выполняется сложный алгоритм
    result = 0
    for i in range(arg1):
        for j in range(arg2):
            with open('test.txt', 'w', encoding='utf8') as file:
                file.write(str(i + j))
                result += i + j
    return result


# Пример вызова функции с применением декоратора
result = complex_algorithm(10, 50)
