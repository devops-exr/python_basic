import functools

user_permissions = ['admin']
#user_permissions = ['user_1']


def check_permission(username: str):
    """
    Декоратор для проверки прав пользователя
    :param username:
    :return: check_permission_sec
    """
    def check_permission_sec(func):
        """Декоратор проверки функций"""
        @functools.wraps(func)
        def wrapper():
            try:
                if username in user_permissions:
                    return func()
                else:
                    raise PermissionError
            except PermissionError as exc:
                print(f'PermissionError: У пользователя недостаточно прав, чтобы выполнить функцию {func.__name__}')

        return wrapper
    return check_permission_sec


@check_permission('admin')
def delete_site():
    print('Удаляем сайт')


@check_permission('user_1')
def add_comment():
    print('Добавляем комментарий')


delete_site()
add_comment()
