import os
from typing import List, Dict, Tuple, Generator


def gen_files_path(dirr: str, fl: str) -> Generator[str, str, str]:
    """
    Функция gen_files_path - выводит файлы в указаном каталоге
    :param dirr:  str (корень папки)
    :param fl:  str (папка, которую нужно найти)
    :yield prt: str (файл в папке)
    """
    print("Search in ", dirr)
    for adress, dirs, files in os.walk(dirr):
        for i in dirs:
            if str(i) == str(fl):
                our_dir = os.path.join(adress, i)
                for adr, dr, fs in os.walk(str(our_dir)):
                    for file in fs:
                        prt = os.path.join(adr, file)
                        yield prt


#enter_file = input("Search file: ")
search = 'Additional_info'
directory = "C:\DevOps\Python_all\Python_2"
for k in gen_files_path(directory, search):
    print(k)
