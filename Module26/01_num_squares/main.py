from typing import List, Dict, Tuple
class Iterator:
    """
    Класс Iterator
    Args:
        n - введенное пользователем число
        counter - счетчик

    """
    def __init__(self, n: int) -> None:
        self.n = n
        self.counter = 0

    def __iter__(self):
        """
        Метод __iter__
        :return: self
        """
        return self

    def __next__(self):
        """
        Метод __next__ - получает следующий єлемент итератора

        :return: self.counter ** 2
        """
        if self.counter < self.n:
            self.counter += 1
            return self.counter ** 2
        else:
            raise StopIteration

def func(n: int) -> str:
    """
    Функция-генератор func(n)
    :param n - число
    :yield count ** 2
    """
    count = 0
    while count < n:
        count += 1
        yield count ** 2


enter = int(input("Bведите число N: "))
funct = (i ** 2 for i in range(1, enter + 1))
first = Iterator(enter)
for i in first:
    print(i, end=" ")
print("\n--------------Second try---------------")

for i in func(enter):
    print(i, end=" ")

print("\n--------------Third try---------------")
for i in funct:
    print(i, end=" ")
