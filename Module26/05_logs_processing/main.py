import os
from typing import List, Dict, Tuple, Optional, Any, Generator


def error_log_generator(input_file_path: str) -> Generator[str, str, str]:
    """

    :param input_file_path: полній путь к файлу
    :yield i - возвращает строку
    """
    with(open(input_file_path, "r", encoding="utf8") as error_file):
        for i in error_file.readlines():
            if i.lstrip().startswith("ERROR:"):
                yield i


main_path = os.getcwd()
# print(main_path)

# При помощи модуля os (и функции join) сформируйте пути до файлов work_logs.txt и output.txt в папке data
# (output.txt может не быть в папке data, но его нужно будет там создать, при помощи кода)
input_file_path = os.path.join(main_path, "data\work_logs.txt")
# print(input_file_path)
output_file_path = os.path.join(main_path, "output.txt")
#print(output_file_path)
# Документация по join https://docs-python.ru/standart-library/modul-os-path-python/funktsija-join-modulja-os-path/

# Не забудьте проверить наличие файлов перед тем как начать работу с ними
# https://docs-python.ru/standart-library/modul-os-path-python/funktsija-exists-modulja-os-path/

if os.path.exists(input_file_path):
    with open(output_file_path, 'w') as output:
        for error_line in error_log_generator(input_file_path):
            output.write(error_line)
    print("Файл успешно обработан.")
else:
    print("Такого файла не существует!")

