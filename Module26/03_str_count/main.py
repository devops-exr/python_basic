import os
from typing import List, Dict, Tuple, Generator


def counter(way: str) -> Generator[str, str, str]:
    """
    Функция counter  - подсчитывает количество строк в файле
    :param way: str (путь)
    :yield: str
    """
    lst = []

    for k in os.listdir(way):
        if k.endswith(".py"):
            lst.append(k)
    #print(lst)

    for j in lst:
        count_lines = 0
        print("-" * 10, os.path.join(way, j), "-" * 10)
        with(open(os.path.join(way, j), 'r', encoding="utf8") as file):
            for i in file.readlines():
                if i.lstrip().startswith("#") or i == "\n":
                    continue
                else:
                    count_lines += 1
        yield os.path.join(way, j), count_lines


way = "C:\DevOps\Python_all\Python_2\practice\ex"
for i, v in counter(way):
    print(f"количество строк в файле {i} === {v}")
    print()
