from typing import List, Dict, Tuple, Optional, Any


class Node:
    """
    Класс Node - узел
    Args:
        value (int) - число
        next (None) - указатель
    """
    def __init__(self, value: int, next: Optional[Any]=None) -> None:
        self.value = value
        self.next = next  # ссылка на след. элем.


class LinkedList:
    """
    Класс LinkedList - связный список
    Args:
        head (int) - начало списка
        length (int) - счетчик
    """
    def __init__(self) -> None:
        self.head = None
        self.length = 0

    def __str__(self) -> Optional[Any]:
        if self.head is not None:
            current = self.head
            result = [str(current.value)]
            while current.next is not None:
                current = current.next
                result.append(str(current.value))
            return str(result)
        return None

    def append(self, element: int) -> None:
        """
        Метод append. Для добавления элемента в связный список
        :param element (int) - элемент, который додавляется в список
        """
        new_elem = Node(element)
        if self.head is None:
            self.head = new_elem
        else:
            current = self.head
            while current.next is not None:
                current = current.next
            current.next = new_elem
        self.length += 1

    def get(self, index: int) -> int:
        """
        Метод get. Возвращает элемент по индексу
        :param index (int) - индекс элемента
        :return: current.value (int)- число
        """
        count = 0
        current = self.head
        if self.length == 0 or index >= self.length:
            raise IndexError
        else:
            while current is not None:
                if index == count:
                    return current.value
                count += 1
                current = current.next
            return current.value

    def remove(self, index: int) -> int:
        """
        Метод remove. Удаляет элемент по индексу
        :param index (int) - индекс элемента, который нужно удалить
        :return current (int)
        """
        count = 0
        current = self.head
        prev = current
        if self.length == 0 or index >= self.length:
            raise IndexError
        if index == 0:
            self.head = self.head.next
            self.length -= 1
            return current
        while current is not None:
            if count == index:
                #prev = current
                break
            else:
                prev = current
                current = current.next
                count += 1

        prev.next = current.next
        self.length -= 1

    # def length(self):
    #     res_len = 0
    #     current = self.head
    #     while current is not None:
    #         res_len += 1
    #         current = current.next
    #     return res_len


my_list = LinkedList()
my_list.append(10)
my_list.append(20)
my_list.append(30)
print('Текущий список:', my_list)
print('Получение третьего элемента:', my_list.get(2))
print('Удаление второго элемента.')
my_list.remove(1)
print('Новый список:', my_list)

