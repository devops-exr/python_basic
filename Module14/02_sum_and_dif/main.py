# TODO здесь писать код
# Напишите две функции. Первая принимает одно целое положительное число N и находит сумму всех цифр в числе.
# Вторая принимает число N и считает количество цифр в числе.
# В ответ выводится разность суммы чисел и количества.
def input_n(N):
    summ = 0
    while N != 0:
        last_num = N % 10
        summ += last_num
        N //= 10
    return summ
def counter(N):
    count = 0
    while N != 0:
        last_num = N % 10
        count += 1
        N //= 10
    return count

num = int(input("Введите число: "))
print("Сумма чисел:", input_n(num))
print("Количество цифр в числе:", counter(num))
result = input_n(num) - counter(num)
print("Разность суммы и количества цифр:", result)


